<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 3 </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-header text-center text-white bg-success">
                 <h4>
                     Division Table
                </h4>   
            </div>
            <div class="card-body p-3">
            <table style="margin-bottom:4px;" class="table table-hover table-md table-bordered  table-dark text-center table-striped">
              <?php
                 echo "<tr>";
                 echo "<th> </th>";

                  $start = 1;
                  $end   = 10;
                   
                  for ($count = $start; $count <= $end; $count++){
                      echo "<th scope ='col'>$count</th>";
                  }

                  echo "</tr>";
                    
                  for ($count = $start; $count <= $end; $count++){
                      
                    echo "<tr>";
                    echo "<th scope ='col'>$count</th>";

                    for($count1 = $start; $count1 <= $end; $count1++){
                        $result = $count / $count1;
                        printf("<td>%.2f</td>", $result);
                    }

                    echo "</tr>";

                }
              ?>
            
            </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>