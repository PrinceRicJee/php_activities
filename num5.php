<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 5 </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card mt-4">
            <div class="card-header text-center text-white bg-success">
                 <h6>
                 Write a program to delete the recurring elements inside a sorted list of integers.
                </h6>
            </div>
            <div class="card-body">
              <?php
                
                //initializing an array of integers
                $integers = array(3,1,7,8,9,56,5,32,1,8,56,100);
                sort($integers);
                print_r($integers);
                
                echo "<br>";
                echo "<br>";
                
                //delete recurring elements
                $del = array_unique($integers);
                print_r($del);

              ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>