<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 2 </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card mt-5 w-25">
            <div class="card-header text-center text-white bg-success">
                 <h6>
                 There are two deals of an item to buy. The quantities and prices of the item are given below. Write a program in PHP to find the best deal to purchase the item.
                </h6>
            </div>
            <div class="card-body">
              <?php
                    $quantity1 = 70;
                    $quantity2 = 100;
                    $price1 = 35;
                    $price2 = 30;
                    
                    $deal1 = $price1 / $quantity1;
                    $deal2 = $price2 / $quantity2;

                    echo "Deal 1 : $deal1<br>";
                    echo "Deal 2 : $deal2<br><hr>";


                    if($deal1 > $deal2){
                        echo "<b>Deal1</b> is the best deal to purchase the item.";
                    }
                    else{
                        echo "<b>Deal2</b> is the best deal to purchase the item.";
                    }
               ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>