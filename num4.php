<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 4 </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card mt-5 w-25">
            <div class="card-header text-center text-white bg-success">
                 <h5>
                 Checking  Armstrong  Number

                </h5>
            </div>
            <div class="card-body">
        
               <form  method="post">
                  <div class="form-group">
                    <label>Input a number: </label>
                    <input type="number" name ="number" class="form-control">
                  </div>
                  <button class="btn btn-primary" name="submit">Submit</button>
               </form>
               <?php

                if(isset($_POST['submit'])){

                    $num = $_POST['number'];
                    $sum = 0;
                    $temp = $num ;
                     
                    if($num == null){

                        echo "<script>alert('Please input a number!');</script>";

                    }

      
                    while ($temp > 0){

                        $remainder = $temp % 10;

                        $sum += $remainder * $remainder * $remainder;

                        $temp = (int)$temp / 10;
                    }

                    echo "<br>";

                    // check the condition
                    if ($num == $sum && $num != null) {

                        echo "<b>$num</b> is an Armstrong number";

                    }
                    if($num != $sum)

                    {
                        echo "<b>$num</b> is not an Armstrong number";
                    }

                }
                ?>

            </div>
        </div>
    </div>
</div>
</body>
</html>