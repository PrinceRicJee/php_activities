<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 7 </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card mt-5 w-25 ">
            <div class="card-header text-center text-white bg-success">
                 <h6>
                    Write a program to convert a digit into its word counterpart. E.g. 721 - Seven Two One
                </h6>
            </div>
            <div class="card-body">
              <div class="form">
               <form method="post">
                  <div class="form-group">
                     <label>Input a number: </label>
                     <input type="number" name="number" class="form-control">
                  </div> 
                     <button name="convert" class="btn btn-primary">Convert</button>
                     <br>
                     <br>

                  
               </form>
              </div>
              <?php

                function getValue($digit){
                 
                    switch($digit){
                        
                        case '0':
                            echo "Zero ";
                            break;
                        case '1':
                            echo "One ";
                            break;
                        case '2':
                            echo "Two ";
                            break;
                        case '3':
                            echo "Three ";
                            break;
                        case '4':
                            echo "Four ";
                            break;
                        case '5':
                            echo "Five ";
                            break;
                        case '6':
                            echo "Six ";
                            break;
                        case '7':
                            echo "Seven ";
                            break;
                        case '8':
                            echo "Eight ";
                            break;
                        case '9':
                            echo "Nine ";
                            break;
                                            
                    }

                }

                function displayWord($number){
                     echo "<b>$number<b> : ";
                    $i = 0;
                    $length = strlen((string)$number);

                    while ($i < $length){

                        getValue($number[$i]);

                        $i += 1;
                    }

                }
                
                if(isset($_POST['convert'])){

                    $num = $_POST['number'];

                    displayWord($num);
                }

              ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>