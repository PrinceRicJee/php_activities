<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 1 </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card mt-5 w-25">
            <div class="card-header text-center text-white bg-success">
                 <h6>
                     Write a program to loop over the given JSON data. Display the values via loops or recursion.
                </h6>
            </div>
            <div class="card-body">
              <?php

                $json = '[
                    {
                        "name" : "John Garg",
                        "age"  : "15",
                        "school" : "Ahlcon Public school"
                    },

                    {
                        "name" : "Smith Soy",
                        "age"  : "16",
                        "school" : "St. Marie school"

                    },
                    {
                        "name" : "Charle Rena",
                        "age"  : "16",
                        "school" : "St. Columba school"

                    }
                ]';

                $arr = json_decode($json,true);

                foreach($arr as $value){
                    echo "<b>Name</b>: ".$value["name"]."<br>";
                    echo "<b>Age</b>: " . $value["age"]."<br>";
                    echo "<b>School</b>: " . $value["school"]."<br>";
                    echo "<hr>";

                }

            ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>